#!/bin/bash

DOT_FILES="/media/raajay/dotfiles"

mkdir -p $DOT_FILES

# vim related stuff
git clone https://github.com/raajay/vim.git $DOT_FILES/dotvim
cd $DOT_FILES/dotvim/ && ./init.sh
ln -s $DOT_FILES/dotvim $HOME/.vim
ln -s $DOT_FILES/dotvim/_vimrc $HOME/.vimrc

# tmux related stuff
git clone https://bitbucket.org/raajay/tmuxinator.git $DOT_FILES/dottmux
ln -s $DOT_FILES/dottmux $HOME/.tmux
ln -s $DOT_FILES/dottmux/_tmux_monokai_conf $HOME/.tmux.conf

# git 
git config --global user.name "Raajay Viswanathan"
git config --global user.email raajay.v@gmail.com