"""
A profile to be for developement purposes. Primarily used for building /
compiling large projects like Tensorflow, Spark, etc.
"""
import os
import geni.portal as portal
import geni.rspec.pg as rspec

request = portal.context.makeRequestRSpec()
node = request.RawPC('dev')
node.disk_image = "urn:publicid:IDN+wisc.cloudlab.us+image+cs-838-big-data-PG0:dev-838:0"
node.addService(
    rspec.Execute(
        shell="sh",
        command="sudo mkdir -p /local/logs"
    )
)
node.addService(
    rspec.Execute(
        shell="sh",
        command=r"sudo /local/repository/scripts/setup-cloudlab-clemson.sh"
    )
)
portal.context.printRequestRSpec()
